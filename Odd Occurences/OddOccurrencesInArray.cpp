#include<iostream>
#include <vector> 
using namespace std;

int solution (vector<int> &A);

int main()
{
    vector<int> example = {4,4,2,10,3,10,3,5,2};
    cout << solution(example);
}

int solution (vector<int> &A)
{
    int result = 0;
    for(auto i : A)
    {
        result ^= i;
    }
    return result;
}