#include<iostream>
#include <vector>
#include <algorithm>
using namespace std;



vector<int> solution(vector<int> &A, int K);

int main()
{
    vector<int> vec = {1,2,3,4,5};
    solution(vec, 6);
    for ( auto i : vec)
    {
        cout << i;
    }
}

vector<int> solution(vector<int> &A, int K)
{
    if(A.empty() || A.size() == 1)
    {
        return A;
    }
    if( K > A.size())
    {
        K = K%A.size();
    }
    rotate(A.begin(), A.begin()+A.size() - K, A.end());
    return A;

}