#include<iostream>
using namespace std;

int solution(int N)
{
    int zeroes = -1;
    int max_gap = 0;
    for ( int i = 0; N>0; i++)
    {   
        if ( N & 1)
        {
            zeroes = 0;
        }
        else if ( zeroes != -1)
        {
            zeroes++;
        }
        N = N >> 1;
        
        if ( max_gap < zeroes)
        {
            max_gap = zeroes;
        }
        
    }
    return max_gap;
}

int main()
{
    cout << solution(32);
}