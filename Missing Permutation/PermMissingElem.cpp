#include <iostream> 
#include <vector>
using namespace std;


int solution(vector<int> &A);

int main()
{
    vector<int> example = {2};
    cout << solution(example);
}

int solution(vector<int> &A)
{
    if(A.size() == 0 || A[0] == 0)
    {
        return 1;
    }
    int expectedsum = 0;
    int observedsum = 0;

    //Since one permutation is missing
    expectedsum = ((A.size() + 1)*(A.size() + 2))/2;
    for (auto i : A)
    {
       observedsum += i;
    }
    int result = expectedsum - observedsum;
    return result;

}