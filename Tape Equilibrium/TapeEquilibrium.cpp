#include<iostream>
#include<math.h>
#include<cmath>
#include<vector>

using namespace std;
int solution (vector<int> &A)
{
    int sumMin = A[0];
    int sumMax = 0;

    for ( int i = 1; i < A.size(); i++)
    {
        sumMax += A[i];
    }

    int minDif = abs(sumMin - sumMax);
    for ( int i = 1; i < A.size() - 1; i++)
    {
        sumMin += A[i];
        sumMax -= A[i];
        minDif = min(minDif, abs(sumMin - sumMax));

    }
    return minDif;
}




int main()
{
    vector<int> example = {3,1,2,4,3};
    cout << solution(example) << endl;
}
