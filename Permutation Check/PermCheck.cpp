#include<iostream>
#include<vector>
#include <bits/stdc++.h> 
using namespace std;

int solution(vector<int> &A)
{
    sort(A.begin(), A.end());
    if(A[0] != 1)
        return 0;
    for(int i = 0; i < A.size() -1  ; i++)
    {
        if(A[i+1] != (A[i]+1))
        {
            return 0;
        }
    }
    return 1;
}
int main()
{
    vector<int> example = {2,3,4,6};
    cout << solution(example) << endl;
}